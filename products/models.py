from django.db import models
from core.models import Core
from django.utils.translation import gettext_lazy as _
from django.urls import reverse, resolve
# Create your models here.


class ProductManager(models.QuerySet):

    def get_queryset():
        return super().get_queryset().get_active()

    def get_active(self):
        return self.filter(active=True)

    def get_slider_trio(self):
        return self.order_by('?')[:3]


class Product(Core):

    """docstring for ClassName"""
    class Meta:
        ordering = ('sort', 'title')
        verbose_name = _('Продукт')
        verbose_name_plural = _('Продукты')

    maker = models.ForeignKey(
        'Producer', verbose_name=_('products'), null=False, blank=False,
        related_name='products', on_delete=models.CASCADE)

    categories = models.ManyToManyField('Category', verbose_name=_('products'), blank=True, related_name='products')

    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)

    objects = ProductManager.as_manager()


class DeletedProduct(Product):
    class Meta:
        proxy = True



class Category(Core):
    class Meta:
        ordering = ('sort', 'title')
        verbose_name = _('Категория')
        verbose_name_plural = _('Категории')


class Producer(Core):
    class Meta:
        ordering = ('sort', 'title')
        verbose_name = _('Производитель')
        verbose_name_plural = _('Производитель')
