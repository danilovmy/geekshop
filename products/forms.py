# -*- coding: utf-8 -*-
# @Author: Max ST
# @Date:   2018-12-09 12:28:09
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-15 15:46:47
# from django import forms
from django import forms
from django.utils.translation import gettext_lazy as _
from django.urls import reverse_lazy
from core.models import Picture
from .models import Product
from django.forms.widgets import CheckboxSelectMultiple
from core.forms import FormHelper
from django.conf import settings
from django.utils.safestring import mark_safe

HELP_TEXT = '<img src="/static/img/icon-unknown.svg" class="helper" style="width:0.5em;height:0.5em;" alt="help" title="{}">'.format


class MyFormset(forms.models.BaseInlineFormSet):
    management_template = '<{tag} name="{prefix}-management_form">{form}</{tag}>'.format
    form_template = '<{tag} name="{prefix}-{index}-form">{form}</{tag}>'.format
    default_render = 'as_ul'

    def add_fields(self, form, index):
        super(MyFormset, self).add_fields(form, index)
        form.fields['DELETE'].widget.attrs['onchange'] = 'rowhidder(this);'

    def render(self, **kwargs):
        forms = [self.render_management()]
        forms += [self.render_form(index, form, **kwargs) for index, form in enumerate(self)]
        return mark_safe(''.join(forms))

    def render_form(self, index, form, **kwargs):
        tag = kwargs.setdefault('tag', 'table')
        method = kwargs.setdefault('method', f'as_{tag}')
        return self.form_template(index=index, form=getattr(form, method)(), prefix=self.prefix, **kwargs)

    def as_table(self):
        "Returns this formset rendered as HTML <table></table>s."
        return self.render(tag='table')

    def as_p(self):
        "Returns this formset rendered as HTML <div></div>s."
        return self.render(tag='div', method='as_p')

    def as_ul(self):
        "Returns this formset rendered as HTML <ul></ul>s."
        return self.render(tag='ul')

    def __str__(self):
        "Returns this formset rendered as self.default_render"
        return getattr(self, self.default_render, self.as_table)()

    def render_management(self):
        return mark_safe(self.management_template(tag='fieldset', prefix=self.prefix, form=str(self.management_form)))



class PictureForm(forms.ModelForm):
    class Meta:
        model = Picture
        fields = ('image', 'title', 'sort')  #'description'


class ProductForm(forms.ModelForm):
    class Media:
        js = ('js/products/formset.js', )
        css = {'all': ('css/products/formset.css',)}

    class Meta:
        model = Product
        fields = ('title', 'description', 'categories', 'maker')
        widgets = {'categories': CheckboxSelectMultiple}


class SearchForm(FormHelper):
    method = 'GET'
    callback = 'filter'
    css_class = 'search'
    submit = None
    title = forms.CharField(max_length=255, min_length=4, required=True, label=_('Строка запроса'), initial=None,
                            help_text=HELP_TEXT(str(_('Поиск товара по имени'))),
                            widget=forms.CharField.widget(attrs={'onchange': 'this.form.submit();',
                                                                 'placeholder': _('что вы хотите найти')}))


SORT_CHOICES = (('title', _('в Алфавитном порядке')),
                ('price', _('сначала дешевые')),
                ('-price', _('сначала дорогие')),
                ('?', _('в случайном порядке')),
                ('', _('все')), )


class SortingForm(FormHelper):
    callback = 'order_by'
    process_url = '/'
    sort = forms.ChoiceField(required=True, label=_('Сортировать по'), choices=SORT_CHOICES, initial='', help_text=_('Сортировка'),
                             widget=forms.ChoiceField.widget(attrs={'onchange': 'this.form.submit();'}))


# class TestPk(forms.Form):
#     method = 'GET'
#     """docstring for SearschForm"""
#     pk = forms.ModelChoiceField(Products.objects, required=True)
