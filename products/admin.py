from django.contrib import admin

# Register your models here.
from .models import Product, DeletedProduct, Producer, Category
from core.models import Picture


class PictureInline(admin.TabularInline):
    model = Picture
    fk_name = 'related_obj'


def clear_category(self, request, queryset):
    for obj in queryset:
        obj.categories.clear()
    messages.info(request, 'категории удалены')


class ProductAdmin(admin.ModelAdmin):
    """docstring for ProductAdmin"""
    inlines = (PictureInline,)
    list_display = ('title', 'price', 'sort', 'active')
    list_editable = ('price', 'sort')
    actions = (clear_category, )

    def get_queryset(self, request):
        return super().get_queryset(request).filter(active=True)


admin.site.register(Product, ProductAdmin)


class ProducerAdmin(admin.ModelAdmin):
    """docstring for ProductAdmin"""
    inlines = (PictureInline,)


admin.site.register(Producer, ProducerAdmin)


class CategoryAdmin(admin.ModelAdmin):
    """docstring for ProductAdmin"""
    inlines = (PictureInline,)


admin.site.register(Category, CategoryAdmin)
