# -*- coding: utf-8 -*-
# @Author: Max ST
# @Date:   2018-12-09 12:28:09
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-15 15:46:47
# from django import forms
from django import forms
from django.contrib.auth.forms import PasswordResetForm, UserCreationForm

from .models import Account


class UserRegisterForm(UserCreationForm, PasswordResetForm):
    class Meta:
        model = Account
        fields = ('username', 'first_name', 'last_name', 'email', 'description', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super(UserRegisterForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'
            field.help_text = ''

    def save(self, commit=True, **kwargs):
        user = self.instance
        if not user.id:
            user.active = False
            user = super(UserRegisterForm, self).save()
            PasswordResetForm.save(self, **kwargs)
        return user


class UserActivationRegisterForm(forms.Form):
    class Meta:
        model = Account

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        self.user.active = True
        if commit:
            self.user.save()
        return self.user
