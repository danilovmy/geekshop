from django.contrib import admin
from django.contrib.auth.models import Permission

# Register your models here.

from .models import Account
# Register your models here.
admin.site.register(Account)
admin.site.register(Permission)