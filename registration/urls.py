# -*- coding: utf-8 -*-
# @Author: MOM
# @Date:   2018-09-26 20:55:31
# @Last Modified by:   Max ST
# @Last Modified time: 2018-12-15 15:11:01
from django.urls import path

from .views import Login, Logout, AccountCreate, AccountConfirm, AccountDetail
                    #, Profile, SignUp, SignUpConfirmView,
                    # changePassword, changeProfile, createAvatarView,
                    # deleteAvatarView)

app_name = 'registration'

urlpatterns = [
    path('login/', Login.as_view(), name='Login'),
    path('logout/', Logout.as_view(), name='Logout'),
    path('singup/', AccountCreate.as_view(), name='AccountCreate'),
    path('confirm/<uidb64>/<token>/', AccountConfirm.as_view(), name='AccountConfirm'),
    path('', AccountDetail.as_view(), name='AccountDetail'),
    # path('editprofile/', changeProfile.as_view(), name='changeProfile'),
    # path('editpass/', changePassword.as_view(), name='changePass'),
    # path('createavatar/<int:related_obj>', createAvatarView.as_view(), name='createAvatar'),
    # path('deleteavatar/<int:pk>/', deleteAvatarView.as_view(), name='deleteAvatar'),
]
