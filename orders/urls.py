"""geekshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

import os
from django.urls import path
from .views import OrderAddProduct, OrderRemoveProduct, InvoiceDetail, InvoiceList, InvoiceClose
from django.conf import settings


app_name = os.path.basename(os.path.dirname(os.path.abspath(__file__)))

urlpatterns = [
    path('<int:ware_id>/add/', OrderAddProduct.as_view(), name='OrderAddProduct'),
    path('<int:ware_id>/remove/', OrderRemoveProduct.as_view(), name='OrderRemoveProduct'),
    path('invoice/', InvoiceDetail.as_view(), name='InvoiceDetail'),
    path('invoice/payment', InvoiceClose.as_view(), name='InvoiceClose'),
    path('', InvoiceList.as_view(), name='InvoiceList'),
]


# only for testing CRUD
if settings.DEBUG:
    from .views import InvoiceCreate, InvoiceDelete, OrderDetail, OrderCreate, OrderDelete
    urlpatterns += [
        path('<int:ware_id>/', OrderDetail.as_view(), name='OrderDetail'),
        path('<int:ware_id>/create/', OrderCreate.as_view(), name='OrderCreate'),
        path('<int:ware_id>/delete/', OrderDelete.as_view(), name='OrderDelete'),
        path('add/', InvoiceCreate.as_view(), {'force': True}, name='InvoiceCreate'),
        path('delete/', InvoiceDelete.as_view(), {'force': True}, name='InvoiceDelete'),
    ]