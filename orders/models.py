from django.db import models
from django.db.models import Q, F, Sum
from django.conf import settings
from products.models import Product
from django.utils.translation import gettext_lazy as _
from core.models import Core, Picture
from django.contrib.sessions.models import Session
# Create your models here.


class DataModel(models.Model):
    class Meta:
        abstract = True
    date = models.DateTimeField(verbose_name=_('Дата создания'), auto_now_add=True, blank=False)
    date_modif = models.DateTimeField(verbose_name=_('Дата модификации'), auto_now=True, blank=True)

    def delete(self, **kwargs):
        return super().delete(force=True)


class InvoiceManager(models.QuerySet):

    def active(self):
        return self.filter(active=True)

    def get_all_from_request(self, request):
        # query = Q(sessions=request.session.session_key)
        return self.filter(owner=request.user) if request.user.is_authenticated else self.none()

    def get_all_from_request_active(self, request):
        return self.get_all_from_request(request).active()

    def get_from_request_active(self, request):
        for invoice in self.get_all_from_request_active(request):
            return invoice


class Invoice(DataModel, Core):
    """docstring for Invoice"""
    class Meta:
        verbose_name = _('Счет заказов')
        verbose_name_plural = _('Счета заказов')

    owner = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Владелец'), null=True, related_name='invoices', on_delete=models.CASCADE)
    status = models.CharField(verbose_name=_('Статус'), max_length=3, choices=[], default='', blank=True)

    objects = InvoiceManager.as_manager()

    def get_summ(self):
        return self.orders.total_summ()

    def get_quantity(self):
        return self.orders.total_quantity()


class OrderManager(models.QuerySet):

    def get_queryset(self):
        return super().get_queryset().active()

    def empty(self):
        return self.active().filter(quantity_lte=0).distinct()

    def active(self):
        return self.filter(is_active=True)

    def get_all_from_request(self, request):
        #query = Q(invoices__sessions=request.session.session_key)
        if request.user.is_authenticated:
            return self.filter(invoices__owner=request.user)
        return self.none()

    def total_quantity(self):
        return self.aggregate(total_quantity=Sum(F('quantity'), output_field=models.IntegerField()))['total_quantity'] or 0

    def total_summ(self):
        return self.aggregate(total_summ=Sum(F('quantity') * F('price'), output_field=models.DecimalField()))['total_summ'] or 0.00


class Order(DataModel, Core):
    """Спецификация Счета"""
    class Meta:
        verbose_name = _('Заказ Продукта')
        verbose_name_plural = _('Заказы Продуктов')

    ware = models.ForeignKey(Product, null=False, on_delete=models.PROTECT)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    quantity = models.PositiveIntegerField(default=0, null=False, blank=True)
    invoices = models.ForeignKey(Invoice, null=False, on_delete=models.CASCADE, related_name='orders')

    objects = OrderManager.as_manager()

    def get_summ(self):
        return (self.price or 0) * (self.quantity or 0)
