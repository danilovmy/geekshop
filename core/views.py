from django.views.generic import ListView, TemplateView, UpdateView, FormView, RedirectView
from .models import Address, Picture
from products.views import ProductList
from django.utils.translation import gettext_lazy as _
from .forms import SendEmailForm
from django.contrib.messages.views import SuccessMessageMixin
from django.template.loader import render_to_string
from django.core.mail import send_mass_mail, send_mail
from django.utils.safestring import mark_safe
from django.conf import settings
from django.contrib import messages
from django.contrib.sites.models import Site

# Create your views here.


class Index(ProductList):
    template_name = 'core/index.html'
    extra_context = {'page_title': _(
        'Магазин'), 'header_class': 'slider', 'content_class': 'featured'}
    order_by = ('?',)
    paginate_by = 3

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['email_form'] = SendEmail(request=self.request).get_form()
        context['paginator'] = self.iterablePaginator(
            context['paginator']).__next__
        return context

    @staticmethod
    def iterablePaginator(paginator):
        for page_num in paginator.page_range:
            yield paginator.get_page(page_num).object_list


class SendEmail(SuccessMessageMixin, FormView):
    form_class = SendEmailForm
    template_name = 'core/index.html'
    fields = ('email', )
    success_url = '/'
    error_message = _(
        'Ваше послание невозможно отправить вам и администрации. Ошибки: {errors}')
    success_message = _('Ваше послание отправлено вам и администрации.')

    def get(self, request, *args, **kwargs):
        self.success_message = ''
        return super().form_valid(form)

    def form_valid(self, form):

        self.template_name = f'core/{type(form).__name__.lower()}_mail.html'
        mail_context = self.get_context_data(
            domain=self.request.site.domain, form=form)
        mail_body = render_to_string(self.get_template_names(), mail_context)
        mail_subject = f'Форма обратной связи на сайте {self.request.site.domain}'
        # (subject, message, from_email, recipient_list)
        datatuple = [(mail_subject, mail_body, None, [
                      settings.EMAIL_HOST_USER, form.cleaned_data['email'], ]), ]
        email_count = send_mass_mail(datatuple, fail_silently=False)

        # breakpoint()
        return super().form_valid(form)

    def form_invalid(self, form):
        self.success_message = self.error_message.format(
            errors=mark_safe(form.errors))
        return super().form_valid(form)


class PictureUpdateView(UpdateView):
    model = Picture
    fields = '__all__'


class AddressList(ListView):
    model = Address
    extra_context = {'page_title': _(
        'Наши контакты'), 'header_class': 'hero', 'content_class': 'location'}
