
from django import forms
from django.utils.translation import gettext_lazy as _
from django.urls import reverse_lazy
from .models import Menu


class FormHelper(forms.Form):
    method = 'POST'
    process_url = ''
    wrapper = True
    submit = {'title': _('Submit'), 'value': 'submit'}
    form_template = 'core/formhelper.html'


class SendEmailForm(FormHelper):
    process_url = reverse_lazy('core:SendEmail')

    # TODO: Define form fields here
    email = forms.EmailField(max_length=255,
                             min_length=5,
                             required=True,
                             label=_('ваш email'),
                             initial=None,
                             help_text=_('никто не поможет ввести вам ваш email кроме вас'))

    # <input type="email" name="email" placeholder="{% trans 'ваш email' %}">