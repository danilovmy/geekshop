from django.db import models
from django.utils.translation import gettext_lazy as _
from django.urls import reverse, resolve
# Create your models here.


class Core(models.Model):

    class Meta:
        ordering = ('sort', 'title')
        verbose_name = _('Ядро')
        verbose_name_plural = _('Ядра')

    title = models.CharField(_('заголовок объекта'), max_length=250, blank=True, null=True)
    description = models.TextField(_('описание объекта'), blank=True, null=True)
    sort = models.IntegerField(_('номер объекта для сортировки'), default=0, blank=True, null=False)
    active = models.BooleanField(_('Активен ли объект'), default=True, db_index=True)

    def __str__(self):
        return f'{self.title}' if self.title else ''

    def delete(self, **kwargs):
        if 'force' in kwargs:
            super().delete()
        else:
            self.active = False
            self.save()

    def get_verbose_name(self):
        return self._meta.verbose_name

    @classmethod
    def get_model_name(cls):
        return cls.__name__.lower()

    @classmethod
    def get_app_name(cls):
        return cls._meta.app_label.lower()

    def get_absolute_url(self):
        return reverse(f'{self.get_app_name()}:{self.get_model_name().capitalize()}Detail', args=[self.pk])

    def get_picture(self):
        for image in self.pictures.all():
            return image



class PictureManager(models.QuerySet):

    def get_queryset(self, **kwargs):
        return self.filter(Q(active=True) | Q(related_obj__isnull=True))

    def get_count(self):
        return self.count()


class Picture(Core):

    class Meta:
        ordering = ('sort', 'title')
        verbose_name = _('Картинка')
        verbose_name_plural = _('Картинки')

    image = models.ImageField(upload_to='pictures')
    related_obj = models.ForeignKey(
        Core, verbose_name=_('pictures'), null=True, blank=True,
        related_name='pictures', on_delete=models.CASCADE)

    objects = PictureManager.as_manager()

    @classmethod
    def get_count_all(cls):
        return cls.objects.get_count()

    def delete(self, **kwargs):
        self.related_obj = None
        super().delete(**kwargs)


class Address(Core):

    class Meta:
        ordering = ('sort', 'title')
        verbose_name = _('Адрес')
        verbose_name_plural = _('Адреса')

    address1 = models.CharField(_('Адрес-street'), max_length=256, blank=True, null=True)
    address2 = models.CharField(_('Адрес-number'), max_length=256, blank=True, null=True)
    zip_code = models.CharField(_('Адрес-zip'), max_length=20, blank=True, null=True)
    city = models.CharField(_('Адрес-city'), max_length=256, blank=True, null=True)
    region = models.CharField(_('Адрес-region'), max_length=256, blank=True, null=True)
    country = models.CharField(_('Адрес-country'), max_length=256, blank=True, null=True)
    phone = models.CharField(_('Адрес-phone'), max_length=256, blank=True, null=True)
    contact_person = models.CharField(_('Адрес-person'), max_length=256, blank=True, null=True)

    related_obj = models.ForeignKey(
        Core, verbose_name=_('Пользователь'), null=True, blank=True,
        related_name='addresses', on_delete=models.CASCADE)

    def get_address(self):
        attrs = ('address1', 'address2', 'zip_code', 'city', 'region', 'country')
        attr_list = (getattr(self, attr, None) or '' for attr in attrs)
        return ' '.join(attr_list)


class MenuManager(models.QuerySet):
    """docstring for MenuManager"""

    def get_menu(self, attr):
        for menu in self.filter(title=attr, parent_id__isnull=True):
            return menu

    def get_supermenu(self):
        return self.filter(parent_id__isnull=True)

    def __getattr__(self, attr):
        attr, splitter, menu = attr.partition('get_menu_')
        if splitter:
            return self.get_menu(menu)
        return super().__getattr__(attr)


class Menu(Core):
    class Meta:
        ordering = ('parent_id', 'sort', 'title')
        verbose_name = _('Элемент меню')
        verbose_name_plural = _('Меню сайта')

    url = models.CharField(_('Линк'), max_length=256, blank=False, null=False, default='index')
    css_class = models.CharField(_('CSS-Класс блока меню'), max_length=30, null=False, blank=True, default='')
    seen_gasts = models.BooleanField(_('Элемент меню виден незарегистрированным пользователям'), default=True)
    seen_users = models.BooleanField(_('Элемент меню виден зарегистрированным пользователям'), default=False)
    parent = models.ForeignKey('self', verbose_name=_('Суперкласс Меню'), null=True, blank=True, related_name='submenus', on_delete=models.CASCADE)
    objects = MenuManager.as_manager()

    def __str__(self):
        self.title = self.title or self.css_class
        return super().__str__()

    def get_title(self):
        return super().__str__()

    def set_css_active(self):
        self.css_class = f'active {self.css_class}'
        return self

    def get_url(self):
        url = self.url
        return reverse(url) if ':' in url else url
